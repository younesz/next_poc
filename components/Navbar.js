import Link from 'next/link';
import React from 'react';

const Navbar = () => {
  return (
    <div className='navbar'>
      <div className='links'>
        <Link href='/'>Home</Link>
        <Link href='/about'>About</Link>
        <Link href='/posts'>Posts</Link>
        <Link href='/blog'>Blog</Link>
      </div>
    </div>
  );
};

export default Navbar;
