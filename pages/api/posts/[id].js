import { posts } from '../../../data';

export default function getPost(req, res) {
  let id = req.query.id;
  let post = posts.find((post) => post.id == id);

  if (post) {
    res.status(200).json(post);
  } else {
    res.status(400).json({ error: 'there is no post' });
  }
}
