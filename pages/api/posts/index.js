import { posts } from '../../../data';

export default function getPosts(req, res) {
  res.status(200).json(posts);
}
