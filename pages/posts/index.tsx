import Link from 'next/link';
import React from 'react';

const Posts = ({ posts }) => {
  return (
    <div className='posts-items d-flex'>
      {posts.map((post) => (
        <Link href={`/posts/${post.id}`} key={post.id}>
          <a className='post-item'>{post.title}</a>
        </Link>
      ))}
    </div>
  );
};

export default Posts;

export const getStaticProps = async (ctx) => {
  const res = await fetch('http://localhost:3000/api/posts');
  const data = await res.json();
  return {
    props: {
      posts: data,
    },
  };
};
