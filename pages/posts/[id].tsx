import React from 'react';
import { useRouter } from 'next/router';

const Post = (props) => {
  const router = useRouter();
  console.log('router ', router);
  return (
    <div className='details'>
      <h1>Post Number {props.post.id}</h1>
      <div>{props.post.body}</div>
    </div>
  );
};

export default Post;

// use this to call each item
// export const getServerSideProps = async (ctx) => {
//   const res = await fetch(
//     `https://jsonplaceholder.typicode.com/posts/${ctx.params.id}`
//   );
//   const data = await res.json();
//   return {
//     props: {
//       post: data,
//     },
//   };
// };

// You should use getStaticPaths if you’re statically pre-rendering pages that use dynamic routes
// use this instead with getStaticPaths
export const getStaticPaths = async (ctx) => {
  const res = await fetch('http://localhost:3000/api/posts');
  const data = await res.json();
  const paths = data.map((d) => {
    return {
      params: {
        id: `${d.id}`,
      },
    };
  });
  return {
    paths: paths,
    fallback: 'blocking',
  };
};
export const getStaticProps = async (ctx) => {
  console.log(ctx);
  const res = await fetch(`http://localhost:3000/api/posts/${ctx.params.id}`);
  const data = await res.json();
  return {
    props: {
      post: data,
    },
  };
};
