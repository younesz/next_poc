import { NextPage } from 'next';
import {
  Button,
  ButtonProps,
  Container,
  styled,
  Typography,
} from '@mui/material';
import EastRoundedIcon from '@mui/icons-material/EastRounded';

import styles from '../../styles/Blog.module.css';

const ColorButton = styled(Button)<ButtonProps>(({ theme }) => ({
  color: 'white',
  borderRadius: '50px',
  padding: '.5rem 3rem',
}));

const Blog: NextPage = () => {
  return (
    <div className={styles.container}>
      <Container fixed>
        <Typography variant='h1Display' component='h1'>
          Create a New Note
        </Typography>
        <Typography variant='h2Display' component='h2'>
          Create a New Note
        </Typography>
        <Typography variant='h3Display' component='h3'>
          Create a New Note
        </Typography>
        <ColorButton
          type='submit'
          variant='contained'
          disableElevation
          endIcon={<EastRoundedIcon fontSize='inherit' />}
          onClick={(e) => {
            e.preventDefault();
            console.log('e clicked ', e);
          }}
        >
          Submit
        </ColorButton>
        <Button variant='contained' color='secondary'>
          Click me
        </Button>
      </Container>
    </div>
  );
};

export default Blog;
