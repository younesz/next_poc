import Link from 'next/link';
import { PrismicProvider } from '@prismicio/react';
import { PrismicPreview } from '@prismicio/next';
import { repositoryName } from '../prismicio';
import type { AppProps } from 'next/app';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { createTheme } from '@mui/material';

import PublicoHeadline from '../public/fonts/PublicoHeadline-Bold.woff2';

import '../styles/globals.css';
import '../main.css';

import Layout from '../components/Layout';

declare module '@mui/material/styles' {
  interface TypographyVariants {
    h1Display: React.CSSProperties;
    h2Display: React.CSSProperties;
    h3Display: React.CSSProperties;
    h4Display: React.CSSProperties;
    h5Display: React.CSSProperties;
    h6Display: React.CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    h1Display?: React.CSSProperties;
    h2Display?: React.CSSProperties;
    h3Display?: React.CSSProperties;
    h4Display?: React.CSSProperties;
    h5Display?: React.CSSProperties;
    h6Display?: React.CSSProperties;
  }
}

// Update the Typography's variant prop options
declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    h1Display: true;
    h2Display: true;
    h3Display: true;
    h4Display: true;
    h5Display: true;
    h6Display: true;
  }
}

const axaTheme = createTheme({
  palette: {
    primary: { main: '#00008F' },
    secondary: { main: '#D24723' },
  },
  typography: {
    htmlFontSize: 16,
    fontSize: 16,
    fontFamily: ['"Source Sans Pro"', 'sans-serif'].join(','),
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 600,
    h1Display: {
      fontFamily: 'PublicoHeadline',
      fontWeight: '600',
      fontSize: '3.5rem',
      lineHeight: '5rem',
      marginTop: 0,
      marginBottom: '0.5rem',
    },
    h2Display: {
      fontFamily: 'PublicoHeadline',
      fontWeight: '500',
      fontSize: '3rem',
      lineHeight: '4.5rem',
      marginTop: 0,
      marginBottom: '0.5rem',
    },
    h3Display: {
      fontFamily: 'PublicoHeadline',
      fontSize: '2.5rem',
      fontWeight: '400',
      lineHeight: '3.5rem',
      marginTop: 0,
      marginBottom: '0.5rem',
    },
  },
  // components: {
  //   MuiCssBaseline: {
  //     styleOverrides: `
  //           @font-face {
  //             font-family: 'PublicoHeadline';
  //             font-style: normal;
  //             font-display: swap;
  //             font-weight: 400;
  //             src: local('PublicoHeadline'), local('PublicoHeadline-Bold'), url(${PublicoHeadline}) format('woff2');
  //             unicodeRange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
  //           }
  //         `,
  //   },
  // },
});

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={axaTheme}>
      <CssBaseline />
      <PrismicProvider
        internalLinkComponent={({ href, ...props }) => (
          <Link href={href}>
            <a {...props} />
          </Link>
        )}
      >
        <PrismicPreview repositoryName={repositoryName}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </PrismicPreview>
      </PrismicProvider>
    </ThemeProvider>
  );
}

export default MyApp;
