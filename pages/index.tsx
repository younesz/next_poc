import { SliceZone } from '@prismicio/react';
import { PrismicRichText } from '@prismicio/react';

import { createClient } from '../prismicio';
import { components } from '../slices';

import styles from '../styles/Home.module.css';

export default function Home({ page }) {
  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <PrismicRichText
          field={page.data.title}
          components={{
            heading1: ({ children }) => (
              <h1 className={styles.title}>
                {children} <a href={page.data.link.url}>Next.js!</a>
              </h1>
            ),
          }}
        />

        <SliceZone slices={page.data.slices} components={components} />
      </main>
    </div>
  );
}
export async function getStaticProps({ previewData }) {
  const client = createClient({ previewData });
  const page = await client.getSingle('home');

  return {
    props: {
      page,
    },
  };
}
